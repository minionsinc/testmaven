package com.prototype.spring;
 
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class StudentDAO {

	private FileWriter writer;
	
	public StudentDAO()
	{
		
	}
	
	public List<Student> getAllStudents()
	{
		List<Student> sl = new LinkedList<Student>();
		
		try{
			Scanner sc = new Scanner(new File("student.csv"));
			
			while(sc.hasNextLine())
			{
				Scanner t = new Scanner(sc.nextLine());
				t.useDelimiter("[\n,]");
				Student s = new Student();
				s.setName(t.next());
				s.setStudentID(t.next());
				s.setALG3(Boolean.parseBoolean(t.next()));
				s.setAP3(Boolean.parseBoolean(t.next()));
				s.setPSD3(Boolean.parseBoolean(t.next()));
				sl.add(s);
				t.close();
			}
			sc.close();
			
		}
		catch(Exception e){}

		return sl;
	}
	public void addToDB(Student s)
	{
		LinkedList<String[]> ll = new LinkedList<String[]>();
		try{
			Scanner sc = new Scanner(new File("student.csv"));
			sc.useDelimiter(",");
			while(sc.hasNextLine())
			{
				String[] temp = new String[1];
				temp[0] = sc.nextLine();
				ll.add(temp);
			}
			sc.close();
		}catch(FileNotFoundException e){}
		try
		{
			writer = new FileWriter("student.csv");
			for(String[] ar : ll)
			{
				addRow(ar);
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		String[] arr = new String[5];
		arr[0] = s.getName();
		arr[1] = s.getStudentID();
		arr[2] = String.valueOf(s.isALG3());
		arr[3] = String.valueOf(s.isAP3());
		arr[4] = String.valueOf(s.isPSD3());
		
		addRow(arr);
		generateCSV();
	}
	
	public void editValue(boolean ALG3, boolean AP3, boolean PSD3)
	{
		try
		{
			for(Student student:getAllStudents())
			{
				String[] arr = new String[5];
				arr[0] = student.getName();
				arr[1] = student.getStudentID();
				arr[2] = String.valueOf(ALG3);
				arr[3] = String.valueOf(AP3);
				arr[4] = String.valueOf(PSD3);
				writer = new FileWriter("student.csv");
				addRow(arr);
			}
			generateCSV();
		}catch(Exception e){}
		
	}
	
	public void generateCSV()
	{
		try
		{
			writer.flush();
			writer.close();
		}
		catch(IOException e)
		{
			 e.printStackTrace();
		} 
	}
	
	public void addRow(String[] row)
	{
		try
		{
			boolean first = true;
			for(String data : row)
			{
				if(!first)
					writer.append(',');
				else
					first = false;
				writer.append(data);
			}
			writer.append("\n");
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}