package com.prototype.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class GeneralController {
	
	private SessionDAO SessionDAO = new SessionDAO();
	private UserDAO userDao = new UserDAO();
	private StudentDAO studentDao = new StudentDAO();
		
	
	@RequestMapping(value="viewGeneral")
	public ModelAndView viewGeneral(HttpServletRequest request) {
		return new ModelAndView("viewGeneral", "SessionDAO", SessionDAO);
	}
	
	@RequestMapping(value="login")
	public ModelAndView login(HttpServletRequest request) {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if(username != null)
		{
			List<User> ulist = userDao.getAllUsers();
			for(User u : ulist)
			{
				if(u.getUsername().equals(username) && u.getPassword().equals(password))
				{
					if(u.getType().equals("admin"))
						return new ModelAndView("redirect:/menu", "userDao", userDao);
					else if(u.getType().equals("user"))
						return new ModelAndView("redirect:/menu2", "userDao", userDao);
				}
			}
			request.setAttribute("message", "Incorrect Username/Password");
		}
		 return new ModelAndView("login", "userDao", userDao);
    }

	@RequestMapping(value="menu")
	public ModelAndView menu(HttpServletRequest request) {
		return new ModelAndView("menu", "userDao", userDao);
	}
	
	@RequestMapping(value="menu2")
	public ModelAndView menu2(HttpServletRequest request) {
		return new ModelAndView("menu2", "userDao", userDao);
	}
	
	@RequestMapping(value="/")
	public ModelAndView home(HttpServletRequest request) {
		return new ModelAndView("login", "userDao", userDao);
	}
	
	@RequestMapping(value="optionalSignup", method=RequestMethod.GET)
    public String loadSessionPage(Model m1) {
		m1.addAttribute("cs", new CreateSession());
        return "optionalSignup";
	}
	
	@RequestMapping(value="optionalSignup", method=RequestMethod.POST)
    public String optionalAttendance(@ModelAttribute CreateSession cs, Model m1) {
    	SessionDAO.editValue(cs.isOptAtt());
        m1.addAttribute("message", "Added");
        m1.addAttribute("cs", new CreateSession());
        return "optionalSignup";
	}
	
	 @RequestMapping(value="CreateSession")
	public ModelAndView session(@ModelAttribute CreateSession cs, HttpServletRequest request) {
		if(request.getAttribute("CreateSession") == null) {
			 request.setAttribute("CreateSession", new CreateSession());
			request.setAttribute("message", "");
		}
		if(request.getParameter("module") != null) {
			SessionDAO.addToDB(cs, "session.csv");
			request.setAttribute("message", "Session Added");
		}
		
		return new ModelAndView("CreateSession", "SessionDAO", SessionDAO);
	}
	 
	@RequestMapping(value="courseSignup", method=RequestMethod.GET)
	public String loadCourseSignup(Model m) {
		m.addAttribute("student", new Student());
	    return "courseSignup";
	}

	@RequestMapping(value="courseSignup", method=RequestMethod.POST)
	public String addCourseSignup(@ModelAttribute Student student, Model m) {
	   	studentDao.editValue(student.isALG3(), student.isAP3(), student.isPSD3());
	   	m.addAttribute("message", "Added");
	    m.addAttribute("student", new Student());
	    return "courseSignup";
	}

	@RequestMapping(value="addStudent", method=RequestMethod.GET)
	public String loadStudentPage(Model m) {
		m.addAttribute("student", new Student());
		return "addStudent";
	}
		
	@RequestMapping(value="addStudent", method=RequestMethod.POST)
	public String createStudent(@ModelAttribute Student student, Model m) {
		studentDao.editValue(student.isALG3(), student.isAP3(), student.isPSD3());
	    m.addAttribute("message", "Added");
	    m.addAttribute("student", new Student());
	    // Prepare the result view (guest.jsp):
	    return "addStudent";
	}
		
	@RequestMapping(value="ViewSession")
	public ModelAndView ViewSession(HttpServletRequest request) {
		return new ModelAndView("viewSession", "SessionDAO", SessionDAO);
	}	
		
	@RequestMapping(value="user", method=RequestMethod.GET)
	public String loadUserPage(Model m) {
		m.addAttribute("user", new User());
	    return "user";
	}
		
	@RequestMapping(value="user", method=RequestMethod.POST)
    public String createUser(@ModelAttribute User user, Model m) {
        if (user.getUsername() != null) {
            userDao.addToDB(new User(user.getUsername(), user.getPassword(), user.getType()));
	        m.addAttribute("message", "Added");
	    }
        else
	      	m.addAttribute("message", "Empty username/password");
	    // Prepare the result view (guest.jsp):
	    return "user";
	}
}
