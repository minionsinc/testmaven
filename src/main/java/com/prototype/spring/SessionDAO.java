package com.prototype.spring;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class SessionDAO {
	
	private FileWriter writer;
	
	public SessionDAO()
	{
	}
	
	public List<CreateSession> getAllSessions(String filename)
	{
		List<CreateSession> sl = new LinkedList<CreateSession>();
		try{
			Scanner sc = new Scanner(new File(filename));
			while(sc.hasNextLine())
			{
				Scanner t = new Scanner(sc.nextLine());
				t.useDelimiter(",");
				CreateSession cs = new CreateSession();
				cs.setModule(t.next());
				cs.setDaydropdown(Integer.parseInt(t.next()));
				cs.setMonthdropdown(t.next());
				cs.setYeardropdown(Integer.parseInt(t.next()));
				cs.setTime(t.next());
				cs.setDuration(Integer.parseInt(t.next()));
				cs.setFrequency(Integer.parseInt(t.next()));
				cs.setMonday(Boolean.parseBoolean(t.next()));
				cs.setTuesday(Boolean.parseBoolean(t.next()));
				cs.setWednesday(Boolean.parseBoolean(t.next()));
				cs.setThursday(Boolean.parseBoolean(t.next()));
				cs.setFriday(Boolean.parseBoolean(t.next()));
				cs.setLecturer(t.next());
				cs.setAttendance(Integer.parseInt(t.next()));
				cs.setCompulsory(Boolean.parseBoolean(t.next()));
				cs.setVenue(t.next());
				cs.setOptAtt(Boolean.parseBoolean(t.next()));
				sl.add(cs);
				t.close();
			}
			sc.close();
		}
		catch(FileNotFoundException e){}

		return sl;
	}
	public void addToDB(CreateSession cs, String filename)
	{
		LinkedList<String[]> ll = new LinkedList<String[]>();
		try{
			Scanner sc = new Scanner(new File(filename));
			sc.useDelimiter(",");
			while(sc.hasNextLine())
			{
				String[] temp = new String[1];
				temp[0] = sc.nextLine();
				ll.add(temp);
			}
			sc.close();
		}catch(FileNotFoundException e){}
		try
		{
			writer = new FileWriter(filename);
			for(String[] ar : ll)
			{
				addRow(ar);
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		String[] arr = new String[17];
		arr[0] = cs.getModule();
		arr[1] = String.valueOf(cs.getDaydropdown());
		arr[2] = cs.getMonthdropdown();
		arr[3] = String.valueOf(cs.getYeardropdown());
		arr[4] = cs.getTime();
		arr[5] = String.valueOf(cs.getDuration());
		arr[6] = String.valueOf(cs.getFrequency());
		arr[7] = String.valueOf(cs.isMonday());
		arr[8] = String.valueOf(cs.isTuesday());
		arr[9] = String.valueOf(cs.isWednesday());
		arr[10] = String.valueOf(cs.isThursday());
		arr[11] = String.valueOf(cs.isFriday());
		arr[12] = cs.getLecturer();
		arr[13] = String.valueOf(cs.getAttendance());
		arr[14] = String.valueOf(cs.isCompulsory());
		arr[15] = cs.getVenue();
		arr[16] = "FALSE";
		
		addRow(arr);
		generateCSV();
	}
	
	public void editValue(boolean optAtt)
	{
		try
		{
			for(CreateSession cs:getAllSessions("lolsession.csv"))
			{
				String[] arr = new String[17];
				arr[0] = cs.getModule();
				arr[1] = String.valueOf(cs.getDaydropdown());
				arr[2] = cs.getMonthdropdown();
				arr[3] = String.valueOf(cs.getYeardropdown());
				arr[4] = cs.getTime();
				arr[5] = String.valueOf(cs.getDuration());
				arr[6] = String.valueOf(cs.getFrequency());
				arr[7] = String.valueOf(cs.isMonday());
				arr[8] = String.valueOf(cs.isTuesday());
				arr[9] = String.valueOf(cs.isWednesday());
				arr[10] = String.valueOf(cs.isThursday());
				arr[11] = String.valueOf(cs.isFriday());
				arr[12] = cs.getLecturer();
				arr[13] = String.valueOf(cs.getAttendance());
				arr[14] = String.valueOf(cs.isCompulsory());
				arr[15] = cs.getVenue();				
				arr[16] = String.valueOf(optAtt);
				writer = new FileWriter("lolsession.csv");
				addRow(arr);
			}
			generateCSV();
		}catch(Exception e){}
		
	}
	
	public void generateCSV()
	{
		try
		{
			writer.flush();
			writer.close();
		}
		catch(IOException e)
		{
			 e.printStackTrace();
		} 
	}
	
	public void addRow(String[] row)
	{
		try
		{
			boolean first = true;
			for(String data : row)
			{
				if(!first)
					writer.append(',');
				else
					first = false;
				writer.append(data);
			}
			writer.append("\n");
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
