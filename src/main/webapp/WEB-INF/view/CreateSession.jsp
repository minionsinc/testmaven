<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
 <%@page import="com.prototype.spring.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
  <head>
    <title>Create a teaching session</title>
    
    <script type="text/javascript">
		var monthtext=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];
		
		function populatedropdown(dayfield, monthfield, yearfield){
		var today=new Date()
		var dayfield=document.getElementById(dayfield)
		var monthfield=document.getElementById(monthfield)
		var yearfield=document.getElementById(yearfield)
		for (var i=0; i<31; i++)
		dayfield.options[i]=new Option(i, i+1)
		dayfield.options[today.getDate()]=new Option(today.getDate(), today.getDate(), true, true) //select today's day
		for (var m=0; m<12; m++)
		monthfield.options[m]=new Option(monthtext[m], monthtext[m])
		monthfield.options[today.getMonth()]=new Option(monthtext[today.getMonth()], monthtext[today.getMonth()], true, true) //select today's month
		var thisyear=today.getFullYear()
		for (var y=0; y<20; y++){
		yearfield.options[y]=new Option(thisyear, thisyear)
		thisyear+=1
		}
		yearfield.options[0]=new Option(today.getFullYear(), today.getFullYear(), true, true) //select today's year
		}
	</script>
	
    <style>
      body { background-color: #eee; font: helvetica; }
      #container { width: 500px; background-color: #fff; margin: 30px auto; padding: 30px; border-radius: 5px; box-shadow: 5px; }
      .green { font-weight: bold; color: green; }
      .message { margin-bottom: 10px; }
      label {width:70px; display:inline-block;}
      form {line-height: 160%; }
      .hide { display: none; }
    </style>
    
  </head>
<body>
     <c:if test="${not empty message}"><div class="message green">${message}</div></c:if>
	 <div id="container">
	 <form:form  modelAttribute= "CreateSession" action="CreateSession">
	  
		<table ALIGN="center" style="width: 467px;">
			<tr>
				<td colspan = 2><h1 align="center">Create A Teaching Session</h1></td>
			</tr>
			
			<tr>
				<td colspan = 2 style="height: 28px; "></td>
			</tr>
			
			<tr>
				<td style="height: 50px; ">Module : </td>
				<td><form:select path="module" style="width: 133px; ">
			    	<option value="DEFAULT">Please Select</option>
			    	<option value="ALG3">ALG3</option>
			    	<option value="ALG3">AP3</option>
			       	<option value="PSD3">PSD 3</option>
				</form:select></td>
			</tr>
			
			<tr>
				<td style="height: 50px; ">Date : </td>
				<td><form:select path="daydropdown" id="daydropdown">
				</form:select> 
				<form:select path="monthdropdown" id="monthdropdown">
				</form:select> 
				<form:select path="yeardropdown" id="yeardropdown">
				</form:select>
				
				<script type="text/javascript">
				//populatedropdown(id_of_day_select, id_of_month_select, id_of_year_select)
				window.onload=function(){
				populatedropdown("daydropdown", "monthdropdown", "yeardropdown")
				}
				</script></td>
				
			</tr>
			
			<tr>
				<td style="height: 50px; ">Time : </td>
				<td><form:input path="time" id="time" style="width: 93px;"></form:input><p style="font-size: small; font-style:italic; display:inline;">(hhhh format)</p></td>
			</tr>
			
			<tr>
				<td style="height: 50px; ">Duration : </td>
				<td><form:input path="duration" id="duration" style="width: 93px;"></form:input> hours</td>
			</tr>
			
			<tr>
				<td valign="top" style="height: 50px; ">Repeat Frequency Per Week : </td>
				<td><form:select path="frequency" id="frequency" size="3" multiple="multiple" tabindex="1" style="width: 166px; height: 93px">
					<option value="1">One day</option>
					<option value="2">Two days</option>
					<option value="3">Three days</option>
					<option value="4">Four days</option>
					<option value="5">Five days</option>
			     	</form:select></td>
			</tr>
			
			<tr>
				<td valign="top" style="height: 50px;">Day : </td>
				<td><form:checkbox path="monday" id="monday"  value=""/>Monday <br />
					<form:checkbox path="tuesday" id="tuesday"  value=""/>Tuesday <br />
					<form:checkbox path="wednesday" id="wednesday"  value=""/>Wednesday <br />
					<form:checkbox path="thursday" id="thursday"  value=""/>Thursday <br />
					<form:checkbox path="friday" id="friday"  value=""/>Friday <br />
				
<!--					<input type="checkbox" name="day" value="Monday">Monday <br />
					<input type="checkbox" name="day" value="Tuesday">Tuesday <br />
					<input type="checkbox" name="day" value="Wednesday">Wednesday <br />
					<input type="checkbox" name="day" value="Thursday">Thursday <br />
					<input type="checkbox" name="day" value="Friday">Friday-->
				</td>
			</tr>
			
			<tr>
				<td style="height: 50px; ">Lecturer : </td>
				<td><form:input path="lecturer"  style="width: 93px;"></form:input></td>
			</tr>
			
			<tr>
				<td style="height: 50px; ">Max Attendance : </td>
				<td><form:input path="attendance"  style="width: 93px;"></form:input></td>
			</tr>
			
			<tr>
				<td style="height: 50px; ">Compulsory : </td>
				<td><form:checkbox path="compulsory" id="compulsory"  value=""/></td>
			</tr>
			
			<tr>
				<td style="height: 50px; ">Venue : </td>
				<td><form:select path="venue" id="venue" style="width: 133px;">
			    	<option value="DEFAULT">Please Select</option>
					<option value="E5">E5</option>
			  		<option value="E6">E6</option>
			    	</form:select></td>
			</tr>
			
			<tr>
				<td colspan = 2 style="height: 71px; ">
					<input type="submit" value="Submit" style="height: 35px; width: 109px;">
				</td>
			</tr>
		</table>
	</form:form>
	</div>

	<hr><ol> 
       <% 
       SessionDAO sessionDao = new SessionDAO();
       for (CreateSession session1 : sessionDao.getAllSessions("session.csv")) { %>
           <li> <%= session1 %> </li>
       <% } %>
       </ol><hr>
  </body>
</html>