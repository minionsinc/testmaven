package com.prototype.spring;

public class Student {
	
	private String name;
	private String studentID;
	private boolean ALG3;
	private boolean AP3;
	private boolean PSD3;
	
	public Student()
	{}
	
	public Student(String name, String studentID, boolean ALG3, boolean PSD3, boolean AP3) {
        this.name = name;
        this.studentID = studentID;
        this.setALG3(ALG3);
        this.setAP3(AP3);
        this.setPSD3(PSD3);
    }
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String username)
	{
		this.name = username;
	}
	
	public String getStudentID()
	{
		return studentID;
	}
	
	public void setStudentID(String studentID)
	{
		this.studentID = studentID;
	}
	
	public boolean isALG3() {
		return ALG3;
	}

	public void setALG3(boolean aLG3) {
		ALG3 = aLG3;
	}

	public boolean isAP3() {
		return AP3;
	}

	public void setAP3(boolean aP3) {
		AP3 = aP3;
	}

	public boolean isPSD3() {
		return PSD3;
	}

	public void setPSD3(boolean pSD3) {
		PSD3 = pSD3;
	}

	@Override
	public String toString() {
		return "Student [Name=" + name + ", studentID=" + studentID + ", ALG3: " 
				+ String.valueOf(ALG3) + ", AP3: " + String.valueOf(AP3) + ", PSD3: " + String.valueOf(PSD3) + "]";
	}
}
