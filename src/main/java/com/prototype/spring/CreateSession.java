package com.prototype.spring;

public class CreateSession {

	private String module;
	private int daydropdown;
	private String monthdropdown;
	private int yeardropdown;
	private String time;
	private int duration;
	private int frequency;
	private boolean monday;
	private boolean tuesday;
	private boolean wednesday;
	private boolean thursday;
	private boolean friday;
	private String lecturer;
	private int attendance;
	private boolean compulsory;
	private String venue;
	private boolean optAtt;
	
	public CreateSession()
	{}
	
	public CreateSession(String module, int daydropdown,String monthdropdown,int yeardropdown,String time,int duration,int frequency,boolean monday,boolean tuesday,boolean wednesday,boolean thursday,boolean friday,String lecturer,int attendance,boolean compulsory,String venue)
	{
		this.module = module;
		this.daydropdown = daydropdown;
		this.monthdropdown = monthdropdown;
		this.yeardropdown = yeardropdown;
		this.time = time;
		this.duration = duration;
		this.frequency = frequency;
		this.monday = monday;
		this.tuesday = tuesday;
		this.wednesday = wednesday;
		this.thursday = thursday;
		this.friday = friday;
		this.lecturer = lecturer;
		this.attendance = attendance;
		this.compulsory = compulsory;
		this.venue = venue;
		
	}
	
	public String getModule() {
		return module;
	}
	
	public void setModule(String module) {
		this.module = module;
	}
	
	public int getDaydropdown() {
		return daydropdown;
	}
	
	public void setDaydropdown(int daydropdown) {
		this.daydropdown = daydropdown;
	}
	
	public String getMonthdropdown() {
		return monthdropdown;
	}
	
	public void setMonthdropdown(String monthdropdown) {
		this.monthdropdown = monthdropdown;
	}
	
	public int getYeardropdown() {
		return yeardropdown;
	}
	
	public void setYeardropdown(int yeardropdown) {
		this.yeardropdown = yeardropdown;
	}
	
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}	
	
	public int getDuration() {
		return duration;
	}
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public int getFrequency() {
		return frequency;
	}
	
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	
	public boolean isMonday() {
		return monday;
	}
	
	public void setMonday(boolean monday) {
		this.monday = monday;
	}
	
	public boolean isTuesday() {
		return tuesday;
	}
	
	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}
	
	public boolean isWednesday() {
		return wednesday;
	}
	
	public void setWednesday(boolean wednesday) {
		this.wednesday = wednesday;
	}

	public boolean isThursday() {
		return thursday;
	}
	
	public void setThursday(boolean thursday) {
		this.thursday = thursday;
	}
	
	public boolean isFriday() {
		return friday;
	}
	
	public void setFriday(boolean friday) {
		this.friday = friday;
	}
	
	public String getLecturer() {
		return lecturer;
	}
	public void setLecturer(String lecturer) {
		this.lecturer = lecturer;
	}	
	
	public int getAttendance() {
		return attendance;
	}
	
	public void setAttendance(int attendance) {
		this.attendance = attendance;
	}
	
	public boolean isCompulsory() {
		return compulsory;
	}
	
	public void setCompulsory(boolean compulsory) {
		this.compulsory = compulsory;
	}
	
	public String getVenue() {
		return venue;
	}
	
	public void setVenue(String venue) {
		this.venue = venue;
	}

	public boolean isOptAtt() {
		return optAtt;
	}
	
	public void setOptAtt(boolean optAtt) {
		this.optAtt = optAtt;
	}
	
	@Override
	public String toString() {
		return "Session [module: " + module + ", daydropdown: " + daydropdown + ", monthdropdown: " + monthdropdown + ", yeardropdown: " + yeardropdown + ", time: " + time + ", duration: " + duration + ", frequency: " + frequency + ", monday: " + String.valueOf(monday) + ", tuesday: " + String.valueOf(tuesday) + ", wednesday: " + String.valueOf(wednesday) + ", thursday: " + String.valueOf(thursday) + ", friday: " + String.valueOf(friday) + ", lecturer: " + lecturer + ", attendance: " + attendance + ", compulsory: " + String.valueOf(compulsory) + ", venue: " + venue + "]";
	}
}
