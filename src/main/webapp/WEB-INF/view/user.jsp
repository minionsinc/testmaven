<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="com.prototype.spring.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:useBean id="userDao" class="com.prototype.spring.UserDAO" scope="request" />
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
    <head>
        <title>User Creation</title>
    </head>
 
    <body>
    <c:if test="${not empty message}"><div class="message green">${message}</div></c:if>
    
        <form:form modelAttribute="user" action="user">
	      <label for="usernameInput">Username: </label>
	      <form:input path="username" id="usernameInput" />
	      <br/>
	      <label for="passwordInput">Password: </label>
	      <form:password path="password" id="passwordInput" />
	      <br/>
	      <label for="typeInput">Password: </label>
	      <form:select path="type" id="typeInput">
	      	<option value="admin">Admin</option>
		  	<option value="user">User</option>
		  </form:select>
	      <br/>
	      <input type="submit" value="Add">
      </form:form>
 
     </body>
 </html>