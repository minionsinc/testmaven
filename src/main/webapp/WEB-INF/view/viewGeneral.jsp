<%@page import="com.prototype.spring.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <style>
      body { background-color: #eee; font: helvetica; }
      #container { width: 800px; background-color: #fff; margin: 30px auto; padding: 30px; border-radius: 5px; box-shadow: 5px; }
      .green { font-weight: bold; color: green; }
      .message { margin-bottom: 10px; }
      label {width:70px; display:inline-block;}
      form {line-height: 160%; }
      .hide { display: none; }
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>General Timetable</title>
</head>
<body>
<div id="container">
<h1>Viewing of General TimeTable </h1>

<table border="1">
<tr>
<th>Module</th>
<th>Date</th>
<th>Time(24Hours)</th>
<th>Duration(Hours)</th>
<th>Frequency</th>
<th>Lecturer</th>
<th>Max Attendance(%)</th>
<th>Compulsory</th>
<th>Venue</th>
</tr>

<% 
        		SessionDAO sessionDao = new SessionDAO();
      			  for (CreateSession session1 : sessionDao.getAllSessions("session.csv")) { %>
					<tr>
						<td><%= session1.getModule() %></td>
						<td><%=   String.valueOf(session1.getDaydropdown())  + String.valueOf(session1.getMonthdropdown()) + String.valueOf(session1.getYeardropdown()) %>
						</td>
						<td><%= session1.getTime() %> hr(s)</td>
						<td><%= session1.getDuration() %> hr(s)</td>
						<td><%= session1.getFrequency() %> day</td>
						<td><%= session1.getLecturer() %></td>
						<td><%= session1.getAttendance() %> %</td>
						<td><%= session1.isCompulsory() %></td>
						<td><%= session1.getVenue() %></td>
		
					</tr>
			<% } %>




</table>


<a href="menu">Back to Main Menu </a>
</div>

</body>

</html>