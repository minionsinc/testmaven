package com.prototype.spring;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class UserDAO {
private FileWriter writer;
	
	public UserDAO()
	{}
	
	public List<User> getAllUsers()
	{
		List<User> ul = new LinkedList<User>();
		
		try{
			Scanner sc = new Scanner(new File("user.csv"));
			
			while(sc.hasNextLine())
			{
				Scanner t = new Scanner(sc.nextLine());
				t.useDelimiter("[\n,]");
				User u = new User();
				u.setUsername(t.next());
				u.setPassword(t.next());
				u.setType(t.next());
				ul.add(u);
				t.close();
			}
			sc.close();
			
		}
		catch(Exception e){}

		return ul;
	}
	public void addToDB(User s)
	{
		LinkedList<String[]> ll = new LinkedList<String[]>();
		try{
			Scanner sc = new Scanner(new File("user.csv"));
			sc.useDelimiter(",");
			while(sc.hasNextLine())
			{
				String[] temp = new String[1];
				temp[0] = sc.nextLine();
				ll.add(temp);
			}
			sc.close();
		}catch(FileNotFoundException e){}
		try
		{
			writer = new FileWriter("user.csv");
			for(String[] ar : ll)
			{
				addRow(ar);
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		String[] arr = new String[3];
		arr[0] = s.getUsername();
		arr[1] = s.getPassword();
		arr[2] = s.getType();
		addRow(arr);
		generateCSV();
	}
	
	public void generateCSV()
	{
		try
		{
			writer.flush();
			writer.close();
		}
		catch(IOException e)
		{
			 e.printStackTrace();
		} 
	}
	
	public void addRow(String[] row)
	{
		try
		{
			boolean first = true;
			for(String data : row)
			{
				if(!first)
					writer.append(',');
				else
					first = false;
				writer.append(data);
			}
			writer.append("\n");
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}