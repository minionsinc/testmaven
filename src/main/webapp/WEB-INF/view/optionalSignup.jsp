<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="com.prototype.spring.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
    
<html>
    <head>
        <title>Optional sign-up</title>
			<style>
				body { background-color: #eee; font: helvetica; }
				#container { width: 500px; background-color: #fff; margin: 30px auto; padding: 30px; border-radius: 5px; box-shadow: 5px; }
				.green { font-weight: bold; color: green; }
				.message { margin-bottom: 10px; }
				label {width:70px; display:inline-block;}
				form {line-height: 160%; }
				.hide { display: none; }
				table { border-collapse: collapse; }
				table.daily th { height: 30px; width: 100px; text-align:center; }
				table.daily td { height: 30px; width: 100px; text-align:center; }
				#monday { display: none }
				#tuesday { display: none }
				#wednesday { display: none }
				#thursday { display: none }
				#friday { display: none }
			</style>
    </head>
    
	<body>
	<c:if test="${not empty message}"><div class="message green">${message}</div></c:if>
			<div id="container">	    
	    	<% StudentDAO studentDao = new StudentDAO(); 
	    	for(Student student:studentDao.getAllStudents()){
	    	if(student.isALG3()==true){
	    	%>
	   		<form:form modelAttribute="cs" action="optionalSignup">
		      <label for="OptALG3">Sign-up for optional ALG3?: </label>
		      <form:checkbox path="optAtt" id="OptALG3"/>
		      <input type="submit" value="Add">
	      	</form:form>
	      	<%}else{%>
	      	There is no optional courses for student to select.
	    	<%}%> 
	    	<%}%> 
        	<hr>
        	</div>
	</body>
</html>