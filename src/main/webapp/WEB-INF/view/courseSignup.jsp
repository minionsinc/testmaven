<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="com.prototype.spring.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
    
<html>
    <head>
        <title>Session sign-up</title>
			<style>
				body { background-color: #eee; font: helvetica; }
				#container { width: 500px; background-color: #fff; margin: 30px auto; padding: 30px; border-radius: 5px; box-shadow: 5px; }
				.green { font-weight: bold; color: green; }
				.message { margin-bottom: 10px; }
				label {width:70px; display:inline-block;}
				form {line-height: 160%; }
				.hide { display: none; }
				table { border-collapse: collapse; }
				table.daily th { height: 30px; width: 100px; text-align:center; }
				table.daily td { height: 30px; width: 100px; text-align:center; }
				#monday { display: none }
				#tuesday { display: none }
				#wednesday { display: none }
				#thursday { display: none }
				#friday { display: none }
			</style>
    </head>
    
	<body>
	<c:if test="${not empty message}"><div class="message green">${message}</div></c:if>
	    	<div id="container">
	        <form:form modelAttribute="student" action="courseSignup">
		      <label for="ALG3Input">Sign-up ALG3?: </label>
		      <form:checkbox path="ALG3" id="ALG3Input"/>
			  <label for="PSD3Input">Sign-up PSD3?: </label>
		      <form:checkbox path="PSD3" id="PSD3Input"/>
			  <label for="AP3Input">Sign-up AP3?: </label>
		      <form:checkbox path="AP3" id="AP3Input"/>
		      <input type="submit" value="Add">
	      </form:form>
	      
	      <hr>
        <% StudentDAO studentDao = new StudentDAO();
        for (Student student : studentDao.getAllStudents()) {%>
            <%= student %>
            <%   }%>
       <hr>
			</div>
        
	</body>
</html>