<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@page import="com.prototype.spring.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Session</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
</script>
<style>
  body { background-color: #eee; font: helvetica; }
  #container { width: 500px; background-color: #fff; margin: 30px auto; padding: 30px; border-radius: 5px; box-shadow: 5px; }
  .green { font-weight: bold; color: green; }
  .message { margin-bottom: 10px; }
  label {width:70px; display:inline-block;}
  form {line-height: 160%; }
  .hide { display: none; }
  table { border-collapse: collapse; }
  table.daily th { height: 30px; width: 100px; text-align:center; }
  table.daily td { height: 30px; width: 100px; text-align:center; }
  #monday { display: none }
  #tuesday { display: none }
  #wednesday { display: none }
  #thursday { display: none }
  #friday { display: none }
</style>
</head>
<body>
<div id="container">

<form id = "group">
<center><h1>View Session</h1></center>

<label>Frequency: </label>
<select id="frequency">
   	<option>Please Select</option>
    <option value="daily">Daily</option>
 	<option value="weekly">Weekly</option>
   	<option value="all">All</option>
</select>

<br />

<div id="daily" style="display:none">
<input type="radio" name="daily" class="daily" value="monday">Monday
<input type="radio" name="daily" class="daily" value="tuesday">Tuesday
<input type="radio" name="daily" class="daily" value="wednesday">Wednesday
<input type="radio" name="daily" class="daily" value="thursday">Thursday
<input type="radio" name="daily" class="daily" value="friday">Friday

<br />

<div id="monday" class="hidden">
	<br />
	<table border="1" class="daily">
		<tr bgcolor="#C8C8C8">
			<th></th><th>Monday</th>
		</tr>	
		<% SessionDAO sessionDao1 = new SessionDAO();
     	for (CreateSession session1 : sessionDao1.getAllSessions("lolsession.csv")) { %>
			<tr>
				<% if (session1.isMonday() == true){ %>
					<td bgcolor="#C8C8C8"><%= session1.getTime() %></td>
					<td><%= session1.getModule() %></td>
				<% } %>
			</tr>
		<% } %>
	</table>
</div>

<div id="tuesday" class="hidden">
	<br />
	<table border="1" class="daily">
		<tr bgcolor="#C8C8C8">
			<th></th><th>Tuesday</th>
		</tr>	
		<% SessionDAO sessionDao2 = new SessionDAO();
     	for (CreateSession session1 : sessionDao2.getAllSessions("lolsession.csv")) { %>
			<tr>
				<% if (session1.isTuesday() == true){ %>
					<td bgcolor="#C8C8C8"><%= session1.getTime() %></td>
					<td><%= session1.getModule() %></td>
				<% } %>
			</tr>
		<% } %>
	</table>
</div>

<div id="wednesday" class="hidden">
	<br />
	<table border="1" class="daily">
		<tr bgcolor="#C8C8C8">
			<th></th><th>Wednesday</th>
		</tr>	
		<% SessionDAO sessionDao3 = new SessionDAO();
     	for (CreateSession session1 : sessionDao3.getAllSessions("lolsession.csv")) { %>
			<tr>
				<% if (session1.isWednesday() == true){ %>
					<td bgcolor="#C8C8C8"><%= session1.getTime() %></td>
					<td><%= session1.getModule() %></td>
				<% } %>
			</tr>
		<% } %>
	</table>
</div>

<div id="thursday" class="hidden">
	<br />
	<table border="1" class="daily">
		<tr bgcolor="#C8C8C8">
			<th></th><th>Thursday</th>
		</tr>	
		<% SessionDAO sessionDao4 = new SessionDAO();
     	for (CreateSession session1 : sessionDao2.getAllSessions("lolsession.csv")) { %>
			<tr>
				<% if (session1.isThursday() == true){ %>
					<td bgcolor="#C8C8C8"><%= session1.getTime() %></td>
					<td><%= session1.getModule() %></td>
				<% } %>
			</tr>
		<% } %>
	</table>
</div>

<div id="friday" class="hidden">
	<br />
	<table border="1" class="daily">
		<tr bgcolor="#C8C8C8">
			<th></th><th>Friday</th>
		</tr>	
		<% SessionDAO sessionDao5 = new SessionDAO();
     	for (CreateSession session1 : sessionDao5.getAllSessions("lolsession.csv")) { %>
			<tr>
				<% if (session1.isFriday() == true){ %>
					<td bgcolor="#C8C8C8"><%= session1.getTime() %></td>
					<td><%= session1.getModule() %></td>
				<% } %>
			</tr>
		<% } %>
	</table>		
</div>
</div>

<div id="weekly" style="display:none">
	<br />
	<table border="1" class="daily">
		<tr bgcolor="#C8C8C8">
			<th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Thursday</th><th>Friday</th>
		</tr>
			
		<% SessionDAO sessionDaoWeek = new SessionDAO();
     	for (CreateSession session1 : sessionDaoWeek.getAllSessions("lolsession.csv")) { %>
     	<tr>
     	<td><% if (session1.isMonday() == true){ %>
     		<%= session1.getModule() %><br />
     		<%= session1.getTime() %>
     	<% } %></td>
     	<td><% if (session1.isTuesday() == true){ %>
     		<%= session1.getModule() %><br />
     		<%= session1.getTime() %>
     	<% } %></td>
     	<td><% if (session1.isWednesday() == true){ %>
     		<%= session1.getModule() %><br />
     		<%= session1.getTime() %>
     	<% } %></td>
     	<td><% if (session1.isThursday() == true){ %>
     		<%= session1.getModule() %><br />
     		<%= session1.getTime() %>
     	<% } %></td>
     	<td><% if (session1.isFriday() == true){ %>
     		<%= session1.getModule() %><br />
     		<%= session1.getTime() %>
     	<% } %></td>     	     	     	
     	</tr>
		<% } %>
	</table>
</div>

<div id="all" style="display:none">
	<p><b>Year 2013/S1</b></p>
	<table border="1" class="daily">
		<% SessionDAO sessionDaoMonth = new SessionDAO();
     	for (CreateSession session1 : sessionDaoMonth.getAllSessions("lolsession.csv")) { %>
			<tr>
				<td bgcolor="#C8C8C8">
				<%= String.valueOf(session1.getDaydropdown())  + "-" + String.valueOf(session1.getMonthdropdown()) %>
				</td>								
				<td><%= session1.getTime() %></td>
				<td><%= session1.getModule() %></td>
			</tr>
		<% } %>	
	</table>
</div>



<script>	
	$('#frequency').change(function(){
	$(this).find("option").each(function()
	{
	$('#' + this.value).hide();
	});
	    $('#' + this.value).show();
	
	});
	
	$(function() {
	    $types = $('.daily');
	    $monday = $('#monday');
	    $tuesday = $('#tuesday');
	    $wednesday = $('#wednesday');
	    $thursday = $('#thursday');
	    $friday = $('#friday');
	    $types.change(function() {
	        $this = $(this).val();
	        if ($this == "monday") {
	            $tuesday.slideUp(200);
	            $wednesday.slideUp(200);
	            $thursday.slideUp(200);
	            $friday.slideUp(200);
	            $monday.delay(200).slideDown(200);
	        }
	        else if ($this == "tuesday") {
	            $monday.slideUp(200);
	            $wednesday.slideUp(200);
	            $thursday.slideUp(200);
	            $friday.slideUp(200);
	            $tuesday.delay(200).slideDown(200);
	        }
	        else if ($this == "wednesday") {
	            $tuesday.slideUp(200);
	            $monday.slideUp(200);
	            $thursday.slideUp(200);
	            $friday.slideUp(200);
	            $wednesday.delay(200).slideDown(200);
	        }
	        else if ($this == "thursday") {
	            $tuesday.slideUp(200);
	            $wednesday.slideUp(200);
	            $monday.slideUp(200);
	            $friday.slideUp(200);
	            $thursday.delay(200).slideDown(200);
	        }
	        else if ($this == "friday") {
	            $tuesday.slideUp(200);
	            $wednesday.slideUp(200);
	            $thursday.slideUp(200);
	            $monday.slideUp(200);
	            $friday.delay(200).slideDown(200);
	        }
	    });
	});
</script>
</form>
</div>
</body>
</html>