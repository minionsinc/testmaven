<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="com.prototype.spring.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
    <head>
        <title>Add new Student</title>
    </head>
 
    <body>
    <c:if test="${not empty message}"><div class="message green">${message}</div></c:if>
    
        <form:form modelAttribute="student" action="addStudent">
	      <label for="nameInput">Name: </label>
	      <form:input path="name" id="nameInput" />
	      <br/>
	      <label for="studentIDInput">Student ID: </label>
	      <form:input path="studentID" id="studentIDInput" />
	      <br/>
	      <label for="ALG3Input">Taking ALG3?: </label>
	      <form:checkbox path="ALG3" id="ALG3Input"/>
		  <label for="AP3Input">Taking AP3?: </label>
	      <form:checkbox path="AP3" id="AP3Input"/>
		  <label for="PSD3Input">Taking PSD3?: </label>
	      <form:checkbox path="PSD3" id="PSD3Input"/>
	      <input type="submit" value="Add">
      </form:form>
 
    	<hr><ol> 
        <% StudentDAO studentDao = new StudentDAO();
        for (Student student : studentDao.getAllStudents()) {%>
            <li> <%= student %> </li>
        <% } %>
        </ol><hr>
 
    	
    	
     </body>
 </html>